package com.entersekt.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.entersekt.dto.BaseResponseDTO;

public class RESTResponse extends ResponseEntity<BaseResponseDTO>{
    
	public static final String RESPONSE_SUCCESS = "200";
    public static final String RESPONSE_FAIL = "300";
    public static final String RESPONSE_ERROR = "500";

    private String responseCode;
    private String responseMessage;
    private HttpStatus httpStatus;
    private BaseResponseDTO data;

    public RESTResponse(String responseCode, String responseMessage, HttpStatus status) {
		super(status);
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.httpStatus = status;
    }

    public ResponseEntity<?> Response() {
    	this.data = new BaseResponseDTO();
		this.data.setResponseCode(responseCode);
		this.data.setResponseMessage(responseMessage);
		return super.status(this.httpStatus).body(this.data);
    }
}

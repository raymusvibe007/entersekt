package com.entersekt.util;

import com.entersekt.dto.FilesResponseDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class FileUtits {
	
	//Logging abstractions can be used instead of this basic implementation
	private final static Logger LOGGER = Logger.getLogger("com.entersekt.util.FileUtits");
	
	final BlockingQueue<String> filesList = new LinkedBlockingQueue<String>();
	
	/*Method to take a path parameter and return the list of required attributes.
	 * @param path to be listed*/
	public List<FilesResponseDTO> processFiles(String path) throws InterruptedException {
		FileExplorer fileExplorer = new FileExplorer(path, this);
        Thread thread = new Thread(fileExplorer);
        thread.start();
        
	    List<Future<FilesResponseDTO>> futures = new ArrayList<Future<FilesResponseDTO>>();
	    Long processStart = System.currentTimeMillis();
	    ExecutorService executors = Executors.newFixedThreadPool(
	        Runtime.getRuntime().availableProcessors());
	    //Serious potential for memory leak here
	    while(true){
	    	String currentPath = this.filesList.take();
	    	if(currentPath.equalsIgnoreCase("")){
	    		break;
	    	}
	        futures.add(executors.submit(new Callable<FilesResponseDTO>(){
	            public FilesResponseDTO call(){
	            	FilesResponseDTO fileObject = new FilesResponseDTO();
					Path path = Paths.get(currentPath);
			        BasicFileAttributes attr;
					try {
						attr = Files.readAttributes(path, BasicFileAttributes.class);
						fileObject.setFileAttributes(attr);
					} catch (IOException e) {
						//Do nothing
						//LOGGER.severe(e.getMessage());
					}
			        fileObject.setFileSize(path.toFile().length());
			        fileObject.setFullPath(currentPath);
	            	return fileObject;
	            }
	        }));
	    }
	    List<FilesResponseDTO> responseList = new ArrayList<FilesResponseDTO>(futures.size());
	    for (Future<FilesResponseDTO> future : futures) {
	    	try {
				responseList.add(future.get());
			} catch (InterruptedException | ExecutionException e) {
				//Skip all failed and continue, can log if we want
				//LOGGER.severe(e.getMessage());
			}
	    }
	    executors.shutdown();
	    Long processEnd = System.currentTimeMillis();
	    LOGGER.info("File processing duration: " + (processEnd - processStart) + " milliseconds.");
	    LOGGER.info("Processes files size: " + responseList.size());
	    return responseList;
	}
}

class FileExplorer implements Runnable {
	
	private final static Logger LOGGER = Logger.getLogger("com.entersekt.util.FileExplorer");
	
	String pathToQuery;
	FileUtits owner;
	public FileExplorer(String path, FileUtits owner) {
		this.pathToQuery = path;
		this.owner = owner;
    }

    public void run() {
    	Long processStart = System.currentTimeMillis();
    	LOGGER.info( "Begin getting files...");
    	getFilesWithDirectoryStream(pathToQuery);
    	this.owner.filesList.add("");
    	Long processEnd = System.currentTimeMillis();
    	LOGGER.info( "Get files duration " + (processEnd - processStart));
    }
	
	/*Recursive method to take a string path parameter and populated the list of file paths belonging to the class instance.
	 * Had trouble with Files.walk around permissions. 
	 * Seems to be a memory leak here or GC not keeping up recursion for LARGE directory sizes
	 * @param path to be listed*/
	private void getFilesWithDirectoryStream(String path){
	    //Path myPath = Paths.get(path);
	    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(path))){  
	            for (Path p : directoryStream) {
	            	if(owner.filesList.size() > 200){
                		try {
							Thread.sleep(100);
							LOGGER.info( "Thread for get files sleeping for 0.1 sec...");
						} catch (InterruptedException e) {
						}
                	}
	                if(p.toFile().isDirectory()){
	                	owner.filesList.add(p.toFile().getAbsolutePath());
	                	getFilesWithDirectoryStream(p.toString());
	                }else{
	                	owner.filesList.add(p.toFile().getAbsolutePath());
	                }
	            }
	            directoryStream.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }finally{
		}
	}
		
	/*Alternate method, way slower and still hits java.lang.OutOfMemoryError: GC overhead limit exceeded*/
	/*private void listf(String directoryName) {
	    File directory = new File(directoryName);

	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.isFile()) {
	        	owner.filesList.add(file.getAbsolutePath());
	        } else if (file.isDirectory()) {
	            listf(file.getAbsolutePath());
	        }
	    }
	}*/
}

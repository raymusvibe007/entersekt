package com.entersekt.dto;

import java.nio.file.attribute.BasicFileAttributes;

public class FilesResponseDTO {
	private String fullPath;
	private Long fileSize; 
	private BasicFileAttributes fileAttributes;
	
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public BasicFileAttributes getFileAttributes() {
		return fileAttributes;
	}
	public void setFileAttributes(BasicFileAttributes fileAttributes) {
		this.fileAttributes = fileAttributes;
	}
}

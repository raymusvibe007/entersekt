package com.entersekt.dto;

public class FilesRequestDTO {
	
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}

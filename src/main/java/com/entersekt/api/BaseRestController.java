package com.entersekt.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.entersekt.util.RESTResponse;

public class BaseRestController {
	
	public BaseRestController() {
	}
	
	protected ResponseEntity<?> ResponseSuccess(String responseMessage) {
		return new RESTResponse(RESTResponse.RESPONSE_SUCCESS, responseMessage, HttpStatus.OK).Response();
    }
	
	protected ResponseEntity<?> ResponseFAIL(String failMessage) {
		return new RESTResponse(RESTResponse.RESPONSE_FAIL, failMessage, HttpStatus.BAD_REQUEST).Response();
    }

    protected ResponseEntity<?> ResponseError(String errorMessage) {
    	return new RESTResponse(RESTResponse.RESPONSE_ERROR, errorMessage, HttpStatus.INTERNAL_SERVER_ERROR).Response();
    }
}

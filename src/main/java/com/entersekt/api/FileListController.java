package com.entersekt.api;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entersekt.dto.FilesRequestDTO;
import com.entersekt.dto.FilesResponseDTO;
import com.entersekt.util.FileUtits;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class FileListController extends BaseRestController{
	
	//Logging abstractions can be used instead of this basic implementation
	private final static Logger LOGGER = Logger.getLogger("com.entersekt.api.FileListController");
	
	private ObjectMapper mapper = new ObjectMapper();
	
	//Application end point to list the directory file contents and their associated attributes as requested
	@RequestMapping(value = "/api/directory/listing", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> GetDirectoryListing(@RequestBody FilesRequestDTO request){
		try{
			FileUtits fileUtiles = new FileUtits();
			Long getFilesStart = System.currentTimeMillis();
			List<FilesResponseDTO> fileResponseList = fileUtiles.processFiles(request.getPath());
			
			//There are many ways to wrap and respond with this result, mapper.writeValueAsString is very fast JSON stringifier
			String jsonString = mapper.writeValueAsString(fileResponseList);
			Long getFilesEnd = System.currentTimeMillis();
			LOGGER.info("Total time: " + (getFilesEnd - getFilesStart) + " milliseconds.");
			
		    return ResponseSuccess(jsonString);
		} catch (Exception e) {
			//You could always do more fine grained exception handling here, and elsewhere, focus was more on optimizing the algorithm.
			LOGGER.log(Level.SEVERE, e.getMessage());
		    return ResponseError(e.toString());
		}
    }
	
	/*@RequestMapping(value = "/api/usr/listing", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> DirectoryListing(){
		try{
			FileUtits fileUtiles = new FileUtits();
			Long getFilesStart = System.currentTimeMillis();
			List<FilesResponseDTO> fileResponseList = fileUtiles.processFiles("/usr");
			
			//There are many ways to wrap and respond with this result, mapper.writeValueAsString is very fast
			String jsonString = mapper.writeValueAsString(fileResponseList);
			Long getFilesEnd = System.currentTimeMillis();
			LOGGER.info("Total time: " + (getFilesEnd - getFilesStart) + " milliseconds.");
			
		    return ResponseSuccess(jsonString);
		} catch (Exception e) {
			//You could always do more fine grained exception handling here, and elsewhere, focus was more on optimizing the algorithm.
			LOGGER.log(Level.SEVERE, e.getMessage());
		    return ResponseError(e.toString());
		}
    }*/
}

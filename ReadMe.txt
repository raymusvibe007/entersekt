1. Open a Linux terminal and run the command "sudo mvn package docker:build" at the "parent" directory containing the pom.xml file.
2. Once complete, run the command "sudo docker run -p 8080:8080 -t springio/gs-spring-boot-docker" at the same directory level. This will fire up the application in a docker container.
3. POST a simple JSON object containing the path/directory you want to search to this URL {IP/localhost}:8080/api/directory/listing. The JSON object must look like this;
	{"path" : "{whatever-path-you-want-to-search}"}, 
	e.g {"path" : "/usr"}
	The JSON response will look like this;
	{
	    "responseCode": "200",
	    "responseMessage": "[]"
	}
	The responseMessage will contain a list of objects depicting the attributes of all the files and folders found in the supplied path/directory
	To POST to the endpoint use an HTTP client like Postman or go to {IP/localhost}:8080/swagger-ui.html
